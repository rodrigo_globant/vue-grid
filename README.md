# vue-template

> Support for ES2015
> Use Webpack to work with  vue files
> Includes Vuetable component


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```


### External Libraries

https://github.com/ratiw/vue-table

https://github.com/vuejs/vue-resource
