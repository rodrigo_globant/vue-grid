
//LIBRARY FOR HTTP CALLS
import VueResource from './vue-resource'
Vue.use(VueResource)


// VUETABLE CONFIG
import Vuetable from './components/Vuetable.vue'
// import VuetablePagination from './components/VuetablePagination.vue'
import VuetablePagination from './components/VuetablePaginationBootstrap.vue'
import PaginationMixin from './components/VuetablePaginationMixin.vue'

Vue.component('vuetable', Vuetable)
Vue.component('vuetable-pagination', VuetablePagination)
